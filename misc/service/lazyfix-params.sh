#!/usr/bin/env bash

### parmaeters 
export COMPOSE_PROJECT_NAME="${COMPOSE_PROJECT_NAME:-harbormaster}" # | BEST-PRACTICE | harbormaster deployment [compose project name](https://docs.docker.com/compose/environment-variables/envvars/#compose_project_name) | harbormaster |

export HARBORMASTER_HOST_CONFIG_ROOT=${HARBORMASTER_HOST_CONFIG_ROOT:-/var/lib/harbormaster/config} # | GOOD-PRACTICE | 'harbormaster.yml' repo clone destination dir in HOST | /var/lib/harbormaster/config |
export HARBORMASTER_HOST_MAIN_ROOT=${HARBORMASTER_HOST_MAIN_ROOT:-/var/lib/harbormaster/main} # | GOOD-PRACTICE | harbormaster data dir for managing apps | /var/lib/harbormaster/main |
export harbormaster_CONFIG_YML_FILENAME=${harbormaster_CONFIG_YML_FILENAME:-harbormaster.yml} # | BEST-PRACTICE | the exact name of 'harbormaster.yml' in config-repo | harbormaster.yml |
export harbormaster_CONFIG_ROOT=${harbormaster_CONFIG_ROOT:-$HARBORMASTER_HOST_CONFIG_ROOT} # | BEST-PRACTICE | container inside bind-mount destination of 'HARBORMASTER_HOST_CONFIG_ROOT' as `-v $HARBORMASTER_HOST_CONFIG_ROOT:$harbormaster_CONFIG_ROOT` | '$HARBORMASTER_HOST_CONFIG_ROOT' ( to use '.' resource in managed apps ) |
export harbormaster_MAIN_ROOT=${harbormaster_MAIN_ROOT:-$HARBORMASTER_HOST_MAIN_ROOT} # | BEST-PRACTICE | container inside bind-mount destination of 'HARBORMASTER_HOST_MAIN_ROOT' as `-v $HARBORMASTER_HOST_MAIN_ROOT:$harbormaster_MAIN_ROOT` | '$HARBORMASTER_HOST_MAIN_ROOT' ( to use '.' resource in managed apps )|

export HBMBOOT_COMPOSE_OPTS=${HBMBOOT_COMPOSE_OPTS:- -f harbormaster-compose.yml} # | GOOD-PRACTICE | docker-compose common options for harbormaster itself | -f harbormaster-compose.yml (e.g. docker-compose *-f harbormaster-compose.yml* up) |
export HBMBOOT_COMPOSE_ON_PREBUILD=${HBMBOOT_COMPOSE_ON_PREBUILD:- build harbormaster bashcmd_at_cfgdir } # | GOOD-PRACTICE | docker-compose subcommand to build in pre-execution phase | build harbormaster bashcmd_at_cfgdir  |
export HBMBOOT_COMPOSE_ON_CONFIGRUN=${HBMBOOT_COMPOSE_ON_CONFIGRUN:- run --rm bashcmd_at_cfgdir /bin/bash -c } # | GOOD-PRACTICE | docker-compose subcommand to run bash in config phase | run --rm bashcmd_at_cfgdir /bin/bash -c |
export HBMBOOT_COMPOSE_ON_START=${HBMBOOT_COMPOSE_ON_START:- up -d --build --remove-orphans} # | GOOD-PRACTICE | docker-compose subcommand for harbormaster to start | up -d --build --remove-orphans (e.g. docker-compose -f harbormaster-compose.yml *up -d --force-recreate --build --remove-orphans*) |
export HBMBOOT_COMPOSE_ON_STOP=${HBMBOOT_COMPOSE_ON_STOP:- stop harbormaster} # | GOOD-PRACTICE | docker-compose subcommand for harbormaster to stop(disable) | stop harbormaster (e.g. docker-compose -f harbormaster-compose.yml *stop harbormaster*) |
export HBMBOOT_COMPOSE_ON_WIPE=${HBMBOOT_COMPOSE_ON_WIPE:- down -v --remove-orphans} # | GOOD-PRACTICE | docker-compose subcommand for harbormaster to get removed | down -v --remove-orphans (e.g. docker-compose -f harbormaster-compose.yml *down -v --remove-orphans*) |

buildin_release_image_name=$(if ! cat /.my-docker-image-name; then echo 'warning: the file /.my-docker-image-name is not found. unknown docker build state' 1>&2 ; echo 'stavros/harbormaster:latest'; fi   )
export HBMBOOT_IMAGE_NAME=${HBMBOOT_IMAGE_NAME:-$buildin_release_image_name} # | BEST-DEFAULT | base harbormaster docker image for boot | (image builtin version) |

export HBMBOOT_DOCKERFILE_CONTEXT=${HBMBOOT_DOCKERFILE_CONTEXT:-./identical.dockerfile/} # | GOOD-PRACTICE | boot image customizer Dockerfile directory | ./identical.dockerfile/ (other example ./gitcrypt.dockerfile or ./remote-init/mycustom.dockerfile/) |