#!/bin/sh

#local builder script for debug

set -eu

CI_COMMIT_TAG=$(git rev-parse --short HEAD)


#cd "$(dirname "$-1")/../"

# Get the version either from CI_COMMIT_TAG if it's defined (ie on CI), or from the
# pyproject.toml otherwise (ie locally).
IMAGE_VERSION="${CI_COMMIT_TAG:-$(sed -n 's/version *= *"\(.*\)\"/\0/p' pyproject.toml)}"
IMAGE_BASE=stavros/harbormaster

echo "Building images"
docker build -f misc/Dockerfile -t "$IMAGE_BASE:$IMAGE_VERSION" -t "$IMAGE_BASE:latest" --build-arg BUILDING_RELEASE_IMGNAME="$IMAGE_BASE:$IMAGE_VERSION" . 
docker build -f misc/Dockerfile.webhook -t "$IMAGE_BASE:${IMAGE_VERSION}-webhook" -t "$IMAGE_BASE:webhook" --build-arg BUILDING_RELEASE_IMGNAME="$IMAGE_BASE:$IMAGE_VERSION-webhook" .