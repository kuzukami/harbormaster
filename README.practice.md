Harbormaster 'practice' Sub-Command
============
Harbormaster requires a very small knowledge footprint and can be started with a minimal Docker-only configuration.
On the other hand, the following requirements can be maximized by planning the deployment.

1. Security (git-crypt or docker-ssh-agent)
1. Adequate File Layout in Docker-Host OS Filesystem
1. Fully Configuration Management For Harbormaster (by git, --remote-init)
1. Detail Network Setup
1. Clean Uninstall (Clean Force Recreate)
1. Version Up for Harbormaster 

To refine the deployment, the '/service/practice' subcommand supports for you to achieve the deployment is controlled only by the overridable parameters by an enviroment variable.

The 'practice' automatically creates and deletes the directories of 'main' and 'config' and boots the harbormaster 'daemon' container, following a generic lifecycle model.
There is no need to issue the host commands for initialization (requires an only docker 1-liner boot).

Additionaly the lifecycle of harbormaster 'daemon' is also controlled by the docker-compose inside the harbormaster container image(host-install-less). Please see the ['Modify compose'](#modify-docker-composeyml-for-harbormaster), [Debug](#debug)  and [Implementations](#implementation-overview) sections if you want to know the detail.


## UsaeCases

### Direct UseCases

```bash
## Use cases. See the script for you to know the overridable env parameters (such as  HBMCONFIG_GIT_REPO).
## Note: Assuming your fixed image is stavros/harbormaster:1c9d488
# [[ UseCase 01. to start ]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro stavros/harbormaster:1c9d488 /service/practice start
# [[ UseCase 02. to stop the harbormaster only where the apps managed by the harbormaster are not stopped. ]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro stavros/harbormaster:1c9d488 /service/practice stop-hm
# [[ UseCase 03. to wipe data related to harbormaster.  ]]
$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:1c9d488 /service/practice wipe-alldata
# [[ UseCase 04. to disable all-applications managed by harbormaster without changing the main config file of harbormaster.yml. ]]
$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:1c9d488 /service/practice disable-allapps
# [[ UseCase 20. direct docker-compose (docker-compose $HBMBOOT_COMPOSE_OPTS "$@") to customize lifecycle of harbormaster entirely. ]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro stavros/harbormaster:1c9d488 /service/practice compose up -d
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro stavros/harbormaster:1c9d488 /service/practice compose down
# [[ UseCase 30. Customize Parameters by using .env file ]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro --env-file your.env.file stavros/harbormaster:1c9d488 /service/practice start
# [[ UseCase 40. Custom Inialization Script in Local ]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro -v "$(pwd)/hbminit.sh:/hbminit.sh:ro" -e HBMBOOT_INIT_SH=/hbminit.sh stavros/harbormaster:1c9d488 /service/practice start
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro -v "$(pwd)/hbminit.sh:/hbminit.sh:ro" -e HBMBOOT_INIT_SH=/hbminit.sh stavros/harbormaster:1c9d488 /service/practice compose up -d
# [[ UseCase 41. customize your boot to change the listening ports of webhook (harbormaster-compose.yml modification) ]] #41
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro -v "$(pwd)/harbormaster-compose.nw.yml:/harbormaster-compose.nw.yml:ro" -e HBMBOOT_COMPOSE_OPTS='-f harbormaster-compose.yml -f /harbormaster-compose.nw.yml' stavros/harbormaster:1c9d488 /service/practice start
# [[ UseCase 42. source the modify-script 'hbminit.sh' in the remote repo before the subcommand operation ]] #42
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro stavros/harbormaster:1c9d488 /service/practice --remote-init start
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro -e HBMBOOT_INIT_SH=/service/remote-init/hbminit.sh  stavros/harbormaster:1c9d488 /service/practice --remote-init compose up -d
# [[ UseCase 43. To Use git-crypt (harbormater image modification) ]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -v /var/run/docker.sock:/var/run/docker.sock:ro -e GIT_CRYPT_KEY_BASE64="AA..areax" -e HBMBOOT_DOCKERFILE_CONTEXT=./gitcrypt.dockerfile stavros/harbormaster:1c9d488 /service/practice start
# [[ UseCase 90. print parameter states for parameter debug ]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:1c9d488 /service/practice print-params
# [[ UseCase 91. print markdown table to describe parameters for document]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:1c9d488 /service/practice print-mddesc
# [[ UseCase 92. print help (this document)]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:1c9d488 /service/practice help
# [[ UseCase 93. view the default harbormaster boot 'harbormaster-compose.yml'  ]]
$ docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:1c9d488 /service/practice cat-harbormaster-compose.yml
```

### Integrated Usecases

`harbormaster inplace version up`
```bash
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:v0.3.0 /service/practice start
bash$ # stop current version and do inplace version up
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:v0.3.0 /service/practice stop-hm
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:v0.3.1 /service/practice start
```

`fully & cleanly redeploy the harbormaster chaging harbormaster parameters(such as branch)`
```bash
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -e HBMCONFIG_GIT_BRANCH=master stavros/harbormaster:v0.3.1 /service/practice start
bash$ #fully remove&stop all containers related to harbormaster
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -e HBMCONFIG_GIT_BRANCH=master stavros/harbormaster:v0.3.1 /service/practice wipe-alldata
bash$ #then restart harbormaster
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git -e HBMCONFIG_GIT_BRANCH=anotherbranch stavros/harbormaster:v0.3.1 /service/practice start
```


## Overridable parameters
Most of the parameters below are set with "good defaults".
Each parameter can be overridden by defining an environment variable of the 'practice' docker container.

| Fix Type | Param Name | State | Description | Default Value (Meaning) |
| ------------------| ---------- | ----- | ----------- | ------------- |
| bootfix-params.sh | HBMCONFIG_GIT_REPO| **MUST-SET!** | your harbormaster.yml git repo(config repo) | **(not set)** |
| bootfix-params.sh | HBMCONFIG_GIT_BRANCH| GOOD-PRACTICE | the deployment branch name of config repo | master |
| bootfix-params.sh | HBMBOOT_INIT_SH| GOOD-PRACTICE | to load initialization bash script in the config repo | /service/remote-init/hbminit.sh (suitable for `--remote-init`) |
| lazyfix-params.sh | COMPOSE_PROJECT_NAME | BEST-PRACTICE | harbormaster deployment [compose project name](https://docs.docker.com/compose/environment-variables/envvars/#compose_project_name) | harbormaster |
| lazyfix-params.sh | HARBORMASTER_HOST_CONFIG_ROOT | GOOD-PRACTICE | 'harbormaster.yml' repo clone destination dir in HOST | /var/lib/harbormaster/config |
| lazyfix-params.sh | HARBORMASTER_HOST_MAIN_ROOT | GOOD-PRACTICE | harbormaster data dir for managing apps | /var/lib/harbormaster/main |
| lazyfix-params.sh | harbormaster_CONFIG_YML_FILENAME | BEST-PRACTICE | the exact name of 'harbormaster.yml' in config-repo | harbormaster.yml |
| lazyfix-params.sh | harbormaster_CONFIG_ROOT | BEST-PRACTICE | container inside bind-mount destination of 'HARBORMASTER_HOST_CONFIG_ROOT' as `-v $HARBORMASTER_HOST_CONFIG_ROOT:$harbormaster_CONFIG_ROOT` | '$HARBORMASTER_HOST_CONFIG_ROOT' ( to use '.' resource in managed apps ) |
| lazyfix-params.sh | harbormaster_MAIN_ROOT | BEST-PRACTICE | container inside bind-mount destination of 'HARBORMASTER_HOST_MAIN_ROOT' as `-v $HARBORMASTER_HOST_MAIN_ROOT:$harbormaster_MAIN_ROOT` | '$HARBORMASTER_HOST_MAIN_ROOT' ( to use '.' resource in managed apps )|
| lazyfix-params.sh | HBMBOOT_COMPOSE_OPTS | GOOD-PRACTICE | docker-compose common options for harbormaster itself | -f harbormaster-compose.yml (e.g. docker-compose *-f harbormaster-compose.yml* up) |
| lazyfix-params.sh | HBMBOOT_COMPOSE_ON_PREBUILD | GOOD-PRACTICE | docker-compose subcommand to build in pre-execution phase | build harbormaster bashcmd_at_cfgdir  |
| lazyfix-params.sh | HBMBOOT_COMPOSE_ON_CONFIGRUN | GOOD-PRACTICE | docker-compose subcommand to run bash in config phase | run --rm bashcmd_at_cfgdir /bin/bash -c |
| lazyfix-params.sh | HBMBOOT_COMPOSE_ON_START | GOOD-PRACTICE | docker-compose subcommand for harbormaster to start | up -d --build --remove-orphans (e.g. docker-compose -f harbormaster-compose.yml *up -d --force-recreate --build --remove-orphans*) |
| lazyfix-params.sh | HBMBOOT_COMPOSE_ON_STOP | GOOD-PRACTICE | docker-compose subcommand for harbormaster to stop(disable) | stop harbormaster (e.g. docker-compose -f harbormaster-compose.yml *stop harbormaster*) |
| lazyfix-params.sh | HBMBOOT_COMPOSE_ON_WIPE | GOOD-PRACTICE | docker-compose subcommand for harbormaster to get removed | down -v --remove-orphans (e.g. docker-compose -f harbormaster-compose.yml *down -v --remove-orphans*) |
| lazyfix-params.sh | HBMBOOT_IMAGE_NAME | BEST-DEFAULT | base harbormaster docker image for boot | (image builtin version) |
| lazyfix-params.sh | HBMBOOT_DOCKERFILE_CONTEXT | GOOD-PRACTICE | boot image customizer Dockerfile directory | ./identical.dockerfile/ (other example ./gitcrypt.dockerfile or ./remote-init/mycustom.dockerfile/) |
| secopt-params.sh | GIT_CRYPT_KEY_BASE64 | OPTIONAL | git-crypt unlock shared key enceded by base64 if set | (not set) |

This table is auto-generated by 'docker run -it --rm -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:latest /service/practice print-mddesc'

| Value | Meaning |
|  --- | --- |
| bootfix-params.sh | Parameter values are fixed & used when practice is started, and should not be overwritten by hbminit.sh for technical reasons. |
| lazyfix-params.sh | Parameter values are fixed & used after the orverwrite in hbminit.sh. |
| secopt-params.sh | Parameter values are optionral parameters, and should not be overwritten byhbminit.sh for security reasons.  |
| **MUST-SET!** | Parameter value must be assigned for boot. |
| BEST-PRACTICE | The default values for the parameters are pretty good and should not be changed unless there is a strong demand.|
| GOOD-PRACTICE | The default values for the parameters are good, but you can change them depending on your needs.|
| BEST-DEFAULT | The default values for the parameters should not be changed for purposes other than debugging.|
| OPTIONAL | Fully optional parameters |


## Custom Configuration

### Usecase 40. Custom init script (HBMBOOT_INIT_SH)

By using the bind-mount '-v' or the [remote-init](#usecase-42-remote-initialization---remote-init) method,
you can put the initialization script `${HBMBOOT_INIT_SH}` whose default value is `/service/remote-init/hbminit.sh`.
Every time 'practice' starts, it executes the script by `source ${HBMBOOT_INIT_SH}` if the script can be read.

Some of the [overridable parameters](#overridable-parameters) can be effectively overwritten by the `${HBMBOOT_INIT_SH}` script, only when 'Fix Type' of the parameter is 'lazyfix-params.sh'.

The working directory, where `source ${HBMBOOT_INIT_SH}` is executed, is '/service' as the default 'practice' boot directory. 

This behavior allows you to manage the 'parameters' or other enviroment variables, and issue arbitrary initialization commands.

### Usecase 41. Modification of docker-compose.yml lifecycle for harbormaster

The 'practice' manages the harbormater 'daemon'(apps-repo-watcher) container by the internally built-in  docker-compose.yml named harbormaster-compose.yml.
The harbormaster-compose.yml file can be printed with the Usecase 93 cat-harbormaster-compose.yml subcommand.

This behavior allows you to change environment variables and issue arbitrary initialization commands.

If you use docker-ssh-agent like docksal/ssh-agent, set portmap for webhook, or set reverse proxy by label with traefik, you need to modify harbormaster-compose.yml.
With Usecase 41, you can specify multiple -f options to modify docker-compose.yml, similar to specifying multiple compose files with docker-compose.


Reference:`harbormaster-compose.yml` at 202305
```bash
bash$ date -u
Mon May  1 02:51:24 UTC 202
bash$
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git  stavros/harbormaster:latest /service/practice cat-harbormaster-compose.yml
version: '3.2'

services:
  bashcmd_at_cfgdir:
    #config bash executer
    build:
      context: "$HBMBOOT_DOCKERFILE_CONTEXT"
      args:
        HBMBOOT_IMAGE_NAME: "$HBMBOOT_IMAGE_NAME"
    restart: unless-stopped
    working_dir: $HARBORMASTER_HOST_CONFIG_ROOT
    environment:
      - HARBORMASTER_HOST_CONFIG_ROOT
      - HARBORMASTER_HOST_MAIN_ROOT
      - harbormaster_CONFIG_ROOT
      - harbormaster_MAIN_ROOT
      - harbormaster_CONFIG_YML_FILENAME
      - GIT_CRYPT_KEY_BASE64=${GIT_CRYPT_KEY_BASE64:-}
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - "${HARBORMASTER_HOST_CONFIG_ROOT}:${harbormaster_CONFIG_ROOT}"
      - "${HARBORMASTER_HOST_MAIN_ROOT}:${harbormaster_MAIN_ROOT}"
    profiles:
      - config.customize
  harbormaster:
    #boot harbormaster itself
    build:
      context: "$HBMBOOT_DOCKERFILE_CONTEXT"
      args:
        HBMBOOT_IMAGE_NAME: "$HBMBOOT_IMAGE_NAME"
    restart: unless-stopped
    environment:
      - HARBORMASTER_HOST_CONFIG_ROOT
      - HARBORMASTER_HOST_MAIN_ROOT
      - harbormaster_CONFIG_ROOT
      - harbormaster_MAIN_ROOT
      - harbormaster_CONFIG_YML_FILENAME
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - "${HARBORMASTER_HOST_CONFIG_ROOT}:${harbormaster_CONFIG_ROOT}"
      - "${HARBORMASTER_HOST_MAIN_ROOT}:${harbormaster_MAIN_ROOT}"
bash$ 
```


### Usecase 42. Remote Initialization (--remote-init)

Is there a way to manage the configuration of harbormaster itself with git?

If you modify harbormaster-compose.yml or change 'practice' parameters, you need to save the settings somewhere.
The UseCase 42 `--remote-init` option exists where it allows you to save those configuration information files to the config.repo where the harbormaster.yml resides.

With `--remote-init`, it temporarily clones the config.repo into '/service/remote-init/' right before the execution of [initialization script](#usecase-40-custom-init-script-hbmboot_init_sh). 
It can also be managed by git in config.repo as well as harbormaster.yml, because the almost all boot 'parameter' including 'harbormaster.yml' configuration can be overridden by the initialization script.
If you pay attention to your working directory, the extension compose files (UseCase 41) for the `harbormaster-compose.yml` file can be managed by git, like this:

`Kickstart Command for 'practice' to use '--remote-init'`
```bash
bash$ #Usecase 42
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:latest /service/practice --remote-init start
bash$ #you can use '--remote-init' with any other subcommands of 'pracitce'
bash$ #docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:latest /service/practice --remote-init stop-hm
bash$ #docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git stavros/harbormaster:latest /service/practice --remote-init wripe-alldata
bash$
bash$ #docker exec harbormaster_harbormaster_1 /bin/ls -la /.verify-remote-init-kuz #verify remote Dockerfile generated file
#-rw-r--r--    1 root     root             0 Apr 20 23:11 /.verify-remote-init-kuz
```

`An example structure of config.repo.`
```bash
bash$ git clone https://example.com/your-hbm-yml.git .
kuzukami@DDCONNY:~/hbmtest$ tree
.
├── bootconfig
│   ├── Dockerfile
│   └── docker-compose.ext.yml
├── harbormaster.yml
└── hbminit.sh

1 directory, 4 files

bash$ cat hbminit.sh 
#!/bin/bash

#harbormaster boot extesion script
#merge the bootconfig/docker-compose.ext.yml into the base harbormaster's compose.yml to boot the harbormaster.
export HBMBOOT_COMPOSE_OPTS="$HBMBOOT_COMPOSE_OPTS -f ./remote-init/bootconfig/docker-compose.ext.yml"

#use a docker image modified by ./bootconfig/Dockerfile for harbormater 'daemon'.
export HBMBOOT_DOCKERFILE_CONTEXT="./remote-init/bootconfig"

bash$ cat bootconfig/Dockerfile 
ARG HBMBOOT_IMAGE_NAME="stavros/harbormaster:latest"
FROM $HBMBOOT_IMAGE_NAME

RUN apk --no-cache add git-crypt
#to verify remote Dockerfile
RUN touch /.verify-remote-init-kuz

bash$ cat bootconfig/docker-compose.ext.yml 
version: '3.2'

services:
  harbormaster:
    ports:
      - "8180:80" #direct access by host:8180 port
    expose:
      - "80"
    labels:
      # traefik label for webhook to be secure
      # see https://doc.traefik.io/traefik/routing/routers/
      - "traefik.enable=true"
      - "traefik.http.routers.hbmwebhook.rule=Host(`hbmwebhook.yourdomain.example.com`)"
      - "traefik.http.routers.hbmwebhook.entrypoints=websecure"
      - "traefik.http.routers.hbmwebhook.tls.certresolver=myresolver"

networks:
  default:
    name: "zzharbormasternw" 

bash$
```

Note that hbminit.sh is only loaded when harbormaster starts.
Even if you update on git, it will not be reflected unless you restart harbormaster.

### Best Practice For Configuration in 'Practice'
In  the "practice" framework, you can adjust your configuration integration applying the Usecases 30,40,41,42 and 43.

```bash
bash$ function hbmpractice(){ subcmd="$1"; shift; docker run -it --rm -v "/var/run/docker.sock:/var/run/docker.sock:ro" "$@"  stavros/harbormaster:latest /service/practice ${REMOTE_OPT:-} "$subcmd"; }

bash$ ##Step 01 Walk through Very Simple Usecase to fix Parameters
bash$ hbmpractice start -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git 

bash$ ##Step 02a use git-crypt(Usecase 43)
bash$ hbmpractice start -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git \
  -e GIT_CRYPT_KEY_BASE64="AA..areax" -e HBMBOOT_DOCKERFILE_CONTEXT=./gitcrypt.dockerfile

bash$ ##Step 02b customize network by merging compose.yml.(Usecase 41)
bash$ hbmpractice start -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git \
  -v "$(pwd)/docker-compose.harbormaster.ext.yml:/service/remote-init/docker-compose.harbormaster.ext.yml:ro" \
  -e HBMBOOT_COMPOSE_OPTS="-f harbormaster-compose.yml -f remote-init/docker-compose.harbormaster.ext.yml"

bash$ ##Step 02c customize harbormasters image by changing docker context of compose.yml(Usecase 43)
bash$ hbmpractice start -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git \
  -v "mycustom/Dockerfile:/service/remote-init/mycustom/Dockerfile" \
  -e HBMBOOT_DOCKERFILE_CONTEXT=./remote-init/mycustom

bash$ ##Step 03 build local 'env-file' and 'hbminit.sh' script(Usecase 40)
bash$ hbmpractice start -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git \
  -v "$(pwd)/docker-compose.harbormaster.ext.yml:/service/remote-init/docker-compose.harbormaster.ext.yml:ro" \
  -v "mycustom/Dockerfile:/service/remote-init/mycustom/Dockerfile" \
  -v "$(pwd)/hbminit.sh:/service/remote-init/hbminit.sh:ro" \
  --env-file=hbm.localmin.env

bash$ cat ./hbm.localmin.env
# minimum enviroments to boot harbormaster
HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git 
# credential should be  in local instead of remote git repo for security.
GIT_CRYPT_KEY_BASE64="AA..areax" 

bash$ cat ./hbminit.sh
#!/bin/sh

#export HBMBOOT_COMPOSE_OPTS='-f harbormaster-compose.yml -f remote-init/docker-compose.harbormaster.ext.yml'
export HBMBOOT_COMPOSE_OPTS="$HBMBOOT_COMPOSE_OPTS -f remote-init/docker-compose.harbormaster.ext.yml"
export HBMBOOT_DOCKERFILE_CONTEXT=./remote-init/mycustom

bash$ # (git commit&push hbminit.sh, docker-compose.harbormaster.ext.yml and mycustom/Dockerfile in the / of config.repo)
bash$ # (rm hbminit.sh, docker-compose.harbormaster.ext.yml and mycustom/Dockerfile in local)

bash$ ##Step 04 '--remote-init' to fetch boot resource from config.repo(Usecase 42)
bash$ REMOTE_OPT='--remote-init' hbmpractice start -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git \
  --env-file=hbm.localmin.env
#same as, and minimum config in local
bash$ docker run -it --rm -v "/var/run/docker.sock:/var/run/docker.sock:ro" --env-file=hbm.loclamin.env stavros/harbormaster:latest /service/practice --remote-init start

```

## Debug
### Parameter Debug
You can debug parameter setting by using Usecase 90.

```bash
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO=https://example.com/your-hbm-yml.git  stavros/harbormaster:latest /service/practice print-params
###################

## HBM BOOTFIX PARAM(cannot be configurable by $HBMBOOT_INIT_SH script)
HBMCONFIG_GIT_REPO='https://example.com/your-hbm-yml.git'
HBMCONFIG_GIT_BRANCH='master'
HBMBOOT_INIT_SH='/service/remote-init/hbminit.sh'

### HBM Security Optional PARAM(config by local envfile is recommended for security)
#'GIT_CRYPT_KEY_BASE64' is not set

## HBM LAZY PARAM(configurable by --remote-init)
COMPOSE_PROJECT_NAME='harbormaster'
HARBORMASTER_HOST_CONFIG_ROOT='/var/lib/harbormaster/config'
HARBORMASTER_HOST_MAIN_ROOT='/var/lib/harbormaster/main'
harbormaster_CONFIG_YML_FILENAME='harbormaster.yml'
harbormaster_CONFIG_ROOT='/var/lib/harbormaster/config'
harbormaster_MAIN_ROOT='/var/lib/harbormaster/main'
HBMBOOT_COMPOSE_OPTS=' -f harbormaster-compose.yml'
HBMBOOT_COMPOSE_ON_PREBUILD=' build harbormaster bashcmd_at_cfgdir '
HBMBOOT_COMPOSE_ON_CONFIGRUN=' run --rm bashcmd_at_cfgdir /bin/bash -c '
HBMBOOT_COMPOSE_ON_START=' up -d --build --remove-orphans'
HBMBOOT_COMPOSE_ON_STOP=' stop harbormaster'
HBMBOOT_COMPOSE_ON_WIPE=' down -v --remove-orphans'
HBMBOOT_IMAGE_NAME='stavros/harbormaster:1c9d488'
HBMBOOT_DOCKERFILE_CONTEXT='./identical.dockerfile/'

###################
bash$
```

### Boot Command Debug (including --remote-init)
The 'practice' logs all the critial 'docker-compose' command for 'daemon' boot and pre/post-process as below.
The security of output is preserved because these commands does not contains the credential data (such as GIT_CRYPT_KEY_BASE64).


```bash
bash$ docker run -it --rm -v '/var/run/docker.sock:/var/run/docker.sock:ro' -e HBMCONFIG_GIT_REPO -e HBMCONFIG_GIT_BRANCH stavros/harbormaster:latest /service/practice --remote-init wipe-alldata
harbormaster-practice exec: docker-compose '-f' 'harbormaster-compose.yml' '-f' './remote-init/bootconfig/docker-compose.ext.yml' 'stop' 'harbormaster'
harbormaster-practice exec: docker-compose '-f' 'harbormaster-compose.yml' '-f' './remote-init/bootconfig/docker-compose.ext.yml' 'run' '--rm' 'bashcmd_at_cfgdir' '/bin/bash' '-c' '( cd $harbormaster_CONFIG_ROOT; rm -rf -- ..?* .[!.]* * ); ( cd $harbormaster_MAIN_ROOT; rm -rf -- ..?* .[!.]* * )'
harbormaster-practice exec: docker-compose '-f' 'harbormaster-compose.yml' '-f' './remote-init/bootconfig/docker-compose.ext.yml' 'down' '-v' '--remove-orphans'
bash$
```



## Implementation Overview
The conceptual bash code is written to clarity, where the paramters are fully default.
The code is very simple.
This document is not primary for the specification.
Please see the code if you want to know the detail implementation.

### practice start
```bash
cd /service

function doinit(){
    export SUB_CMD="$1";
    if [ -r "/service/remote-init/hbm-init.sh" ];then
        source "/service/remote-init/hbm-init.sh" #HBMBOOT_INIT_SH
    fi
}
function doprebuild(){
  docker-compose -f harbormaster-compose.yml build harbormaster bashcmd_at_cfgdir  #HBMBOOT_COMPOSE_ON_PREBUILD
}

function bashcmd_at_cfgdir(){
  docker-compose -f harbormaster-compose.yml  run --rm bashcmd_at_cfgdir /bin/bash -c "$1" #HBMBOOT_COMPOSE_ON_CONFIGRUN
}
function dostart(){
	#clone or pull for config.repo
	bashcmd_at_cfgdir  "git -C . pull || git clone -b master https://example.com/your-hbm-yml.git ."

	if printenv GIT_CRYPT_KEY_BASE64 > /dev/null; then
		#git-crypt shared key usecase
    bashcmd_at_cfgdir "apk --no-cache add git-crypt && git-crypt unlock <(printenv GIT_CRYPT_KEY_BASE64 | base64 -d )"
	fi

	#start harbormaster 
	docker-compose -f harbormaster-compose.yml up -d --build --remove-orphans #HBMBOOT_COMPOSE_ON_START
}

doinit start
doprebuild
dostart

```

### practice --remote-init start
```bash
cd /service
function cloneremote(){
  mkdir -p /service/remote-init
	git clone -b master https://example.com/your-hbm-yml.git /service/remote-init
}

cloneremote
doinit start
doprebuild
dostart
```

### practice --remote-init stop-hm
```bash
cd /service
cloneremote
doinit stop-hm
doprebuild
docker-compose -f harbormaster-compose.yml stop harbormaster
```
### practice --remote-init disable-allapps
```bash
cd /service
cloneremote
doinit disable-allapps
#stop before disable-all
docker-compose -f harbormaster-compose.yml stop harbormaster
#disable all apps by using special command of harbormaster
bashcmd_at_cfgdir "/usr/bin/run-harbormaster disable-all"
```
### practice --remote-init wipe-alldata
```bash
cd /service
cloneremote
doinit wipe-alldata
doprebuild

#stop before wipe
docker-compose -f harbormaster-compose.yml stop harbormaster

#disable all apps by using special command of harbormaster before wipe
bashcmd_at_cfgdir "/usr/bin/run-harbormaster disable-all"

#to clean related files
bashcmd_at_cfgdir "( cd /var/lib/harbormaster/config; rm -rf -- * ); ( cd /var/lib/harbormaster/master ; rm -rf -- * )"

#wipe data
docker-compose -f harbormaster-compose.yml down -v --remove-orphans

```